# -*- coding: utf-8 -*-
from scenes_test_base import ScenesTestBase
import tests_data
from scenes import raster, vector
from scenes.spatial import BoundingBox
import scenes
import pyotb
from osgeo import gdal

sc = scenes.spot.get_local_spot67drs_scene(dimap_xs=tests_data.DIMAP1_XS, dimap_pan=tests_data.DIMAP1_P)


class SpatialTest(ScenesTestBase):
    BBOX_REF = BoundingBox(xmin=4.3153933040851165,
                           xmax=4.421982273366675,
                           ymin=43.6279867026818,
                           ymax=43.70827724313909)

    def compare_bboxes(self, bbox1, bbox2, eps=1e-7):
        coords = [(bbox1.xmin, bbox2.xmin),
                  (bbox1.xmax, bbox2.xmax),
                  (bbox1.ymin, bbox2.ymin),
                  (bbox1.ymax, bbox2.ymax)]
        for v1, v2 in coords:
            self.assertTrue(abs(v1 - v2) < eps)

    def test_bbox(self):
        assert isinstance(raster.get_bbox_wgs84(sc.get_xs()), BoundingBox)
        assert isinstance(vector.get_bbox_wgs84(tests_data.ROI_MTP_4326), BoundingBox)

    def test_otbinput_bbox(self):
        img = pyotb.Input(tests_data.DIMAP1_XS)
        bbox_otb = raster.get_bbox_wgs84(img)
        self.compare_bboxes(bbox_otb, self.BBOX_REF)

    def test_otbapp_bbox(self):
        img = pyotb.DynamicConvert(sc.get_xs())
        bbox_otb = raster.get_bbox_wgs84(img)
        self.compare_bboxes(bbox_otb, self.BBOX_REF)

    def test_gdal_ds_bbox(self):
        gdal_ds = gdal.Open(tests_data.DIMAP1_XS)
        bbox_gdal = raster.get_bbox_wgs84(gdal_ds)
        self.compare_bboxes(bbox_gdal, self.BBOX_REF)

    def test_filename_bbox(self):
        bbox_file = raster.get_bbox_wgs84(tests_data.DIMAP1_XS)
        self.compare_bboxes(bbox_file, self.BBOX_REF)


if __name__ == '__main__':
    unittest.main()
