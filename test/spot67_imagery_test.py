# -*- coding: utf-8 -*-
from functools import singledispatch
from typing import Any
import pyotb

import scenes
import tests_data
from scenes_test_base import ScenesTestBase

# Baseline
xs_dn_ref = tests_data.DIMAP1_XS
p_dn_ref = tests_data.DIMAP1_P
xs_toa_ref = pyotb.OpticalCalibration(tests_data.DIMAP1_XS)
p_toa_ref = pyotb.OpticalCalibration(tests_data.DIMAP1_P)
roi = [2500, 2500, 500, 500]  # For pxs we test the central area only, because
# bayes method messes with nodata outside image boundaries
pyotb_pxs = pyotb.BundleToPerfectSensor
pxs_dn_ref = pyotb_pxs({"inxs": xs_dn_ref, "inp": p_dn_ref})
pxs_toa_ref = pyotb_pxs({"inxs": xs_toa_ref, "inp": p_toa_ref})

# Inputs
sc = scenes.spot.get_local_spot67drs_scene(
    dimap_xs=tests_data.DIMAP1_XS,
    dimap_pan=tests_data.DIMAP1_P
)
xs = sc.get_xs()
pan = sc.get_pan()
pxs = sc.get_pxs()


@singledispatch
def subset(inp: Any) -> Any:
    raise TypeError(
        "Invalid type, must be one of: pyotb.core.OTBObject, Source"
    )


@subset.register
def subset_source(inp: scenes.core.Source):
    return inp.subset(
        startx=roi[0],
        sizex=roi[0] + roi[2],
        starty=roi[1],
        sizey=roi[1] + roi[3]
    )


@subset.register
def subset_pyotb(inp: pyotb.core.OTBObject):
    return inp[roi[0]:roi[0] + roi[2], roi[1]:roi[1] + roi[3], :]


class Spot67ImageryTest(ScenesTestBase):

    def test_instanciate_sc(self):
        assert sc

    def test_xs_dn(self):
        self.compare_images(image=xs, reference=xs_dn_ref)

    def test_xs_toa(self):
        self.compare_images(image=xs.reflectance(), reference=xs_toa_ref)

    def test_pan_dn(self):
        self.compare_images(image=pan, reference=p_dn_ref)

    def test_pan_toa(self):
        self.compare_images(image=pan.reflectance(), reference=p_toa_ref)

    def test_pxs_dn(self):
        self.compare_images(image=pxs, reference=pxs_dn_ref, roi=roi)

    def test_pxs_toa(self):
        self.compare_images(
            image=pxs.reflectance(), reference=pxs_toa_ref, roi=roi
        )

    def test_pxs_dn_msk_drilled(self):
        """
        Dummy test since no cloud in the input scene.
        This test just checks that the code is running but not guarantee that
        the results are good.
        """
        self.compare_images(
            image=pxs.cld_msk_drilled(), reference=pxs_dn_ref, roi=roi
        )

    def test_pxs_toa_msk_drilled(self):
        """
        Dummy test since no cloud in the input scene.
        This test just checks that the code is running but not guarantee that
        the results are good.
        """
        self.compare_images(
            image=pxs.reflectance().cld_msk_drilled(),
            reference=pxs_toa_ref,
            roi=roi
        )

    def test_pxs_dn_msk_drilled_cached(self):
        """
        Dummy test since no cloud in the input scene.
        This test just checks that the code is running but not guarantee that
        the results are good.
        """
        for _ in range(2):
            self.compare_images(
                image=subset(pxs).cld_msk_drilled().cached(),
                reference=subset(pxs_dn_ref)
            )

    def test_pxs_toa_msk_drilled_cached(self):
        """
        Dummy test since no cloud in the input scene.
        This test just checks that the code is running but not guarantee that
        the results are good.
        """
        for _ in range(2):
            self.compare_images(
                image=subset(pxs).reflectance().cld_msk_drilled().cached(),
                reference=subset(pxs_toa_ref),
            )

    def test_print(self):
        print(sc)
        print(xs)
        print(pan)
        print(pxs)


if __name__ == '__main__':
    # unittest.main()
    Spot67ImageryTest().test_pxs_toa_msk_drilled_cached()
    Spot67ImageryTest().test_pxs_dn_msk_drilled_cached()
    Spot67ImageryTest().test_pxs_toa_msk_drilled()
    Spot67ImageryTest().test_pxs_dn_msk_drilled()
