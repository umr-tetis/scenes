# -*- coding: utf-8 -*-
import os
from scenes_test_base import ScenesTestBase
import scenes
import tests_data
import tempfile

sc_spot67 = scenes.spot.get_local_spot67drs_scene(
    dimap_xs=tests_data.DIMAP1_XS,
    dimap_pan=tests_data.DIMAP1_P
)
sc_s2a = scenes.sentinel.Sentinel22AScene(archive=tests_data.ARCHIVE1)
sc_s3a = scenes.sentinel.Sentinel23AScene(archive=tests_data.ARCHIVE2)
scs = [sc_spot67, sc_s2a, sc_s3a]

class SerializationTest(ScenesTestBase):

    def compare_scenes(self, sc1, sc2):
        assert sc1.metadata == sc2.metadata

    def test_serialize(self):
        for sc in scs:
            with tempfile.NamedTemporaryFile() as tmp:
                pickle_file = tmp.name
                scenes.save_scenes(scenes_list=[sc], pickle_file=pickle_file)
                assert os.path.isfile(pickle_file), \
                    f"File {pickle_file} not found!"

    def test_deserialize(self):
        for sc in scs:
            with tempfile.NamedTemporaryFile() as tmp:
                pickle_file = tmp.name
                scenes.save_scenes(scenes_list=[sc], pickle_file=pickle_file)
                loaded_scs = scenes.load_scenes(pickle_file=pickle_file)
                assert len(loaded_scs)==1, \
                    f"There is {len(loaded_scs)} scenes " \
                    f"in {pickle_file}! (expected 1)"
                self.compare_scenes(sc1=loaded_scs[0], sc2=sc)


if __name__ == '__main__':
    unittest.main()
