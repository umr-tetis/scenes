# -*- coding: utf-8 -*-
import tempfile
from scenes_test_base import ScenesTestBase
import tests_data
import scenes
import pyotb


def temp_file():
    return tempfile.NamedTemporaryFile(suffix=".tif").name


def concat(input_list):
    return pyotb.ConcatenateImages({"il": input_list})


sc_2a = scenes.sentinel.Sentinel22AScene(archive=tests_data.ARCHIVE1)
sc_3a = scenes.sentinel.Sentinel23AScene(archive=tests_data.ARCHIVE2)
scs = [sc_2a, sc_3a]
all_10m_bands = ["b4", "b3", "b2", "b8"]
all_20m_bands = ["b5", "b6", "b7", "b8a", "b11", "b12"]


class Sentinel2TheiaImageryTest(ScenesTestBase):

    def instantiate_theia_2a(self):
        assert sc_2a

    def instantiate_theia_3a(self):
        assert sc_3a

    def test_bands(self):
        for sc in scs:
            for getter in [sc.get_10m_bands, sc.get_20m_bands]:
                assert getter()

    def test_write_bands_all(self):
        tmp = temp_file()
        for sc in scs:
            sc.get_10m_bands().write(tmp)
            self.compare_images(tmp, concat([sc.assets_paths[key] for key in all_10m_bands]))
            sc.get_20m_bands().write(tmp)
            self.compare_images(tmp, concat([sc.assets_paths[key] for key in all_20m_bands]))

    def test_write_bands_selection(self):
        tmp = temp_file()
        for sc in scs:
            sc.get_10m_bands("b4").write(tmp)
            self.compare_images(tmp, sc.assets_paths["b4"])
            sc.get_10m_bands("B4").write(tmp)
            self.compare_images(tmp, sc.assets_paths["b4"])
            sc.get_10m_bands(["b4"]).write(tmp)
            self.compare_images(tmp, sc.assets_paths["b4"])
            sc.get_20m_bands("b5").write(tmp)
            self.compare_images(tmp, sc.assets_paths["b5"])
            sc.get_20m_bands(["b5", "b7"]).write(tmp)
            self.compare_images(tmp, concat([sc.assets_paths["b5"], sc.assets_paths["b7"]]))
            sc.get_20m_bands("b5", "b7").write(tmp)
            self.compare_images(tmp, concat([sc.assets_paths["b5"], sc.assets_paths["b7"]]))

    def test_write_single_band(self):
        tmp = temp_file()
        for sc in scs:
            for band in all_10m_bands + all_20m_bands:
                get_band = getattr(sc, f"get_{band}")
                get_band().write(tmp)
                self.compare_images(tmp, sc.assets_paths[band])

    def test_cld_write(self):
        # TODO: compare with baseline
        tmp = temp_file()
        sc_2a.get_10m_bands().cld_msk_drilled().write(tmp)
        sc_2a.get_20m_bands().cld_msk_drilled().write(tmp)

    def test_flg_write(self):
        # TODO: compare with baseline
        tmp = temp_file()
        sc_3a.get_10m_bands().flg_msk_drilled().write(tmp)
        sc_3a.get_20m_bands().flg_msk_drilled().write(tmp)

    def test_print(self):
        for sc in scs:
            print(sc)
            for getter in [sc.get_10m_bands, sc.get_20m_bands]:
                print(getter())


if __name__ == '__main__':
    unittest.main()
