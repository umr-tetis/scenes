# -*- coding: utf-8 -*-
import planetary_computer
import dinamis_sdk
from pystac_client import Client

from scenes.stac import from_stac
from scenes_test_base import ScenesTestBase
from scenes.sentinel import Sentinel2MPCScene
from scenes.spot import Spot67DRSScene


class StacTest(ScenesTestBase):

    def test_planetary(self):
        api = Client.open(
            'https://planetarycomputer.microsoft.com/api/stac/v1',
            modifier=planetary_computer.sign_inplace,
        )
        res = api.search(
            collections=["sentinel-2-l2a"],
            bbox=[4.99, 43.99, 5, 44.01],
            datetime=['2020-01-01', '2020-01-08']
        )

        for item in res.items():
            sc = from_stac(item)
            assert isinstance(sc, Sentinel2MPCScene)

    def test_dinamis(self):
        api = Client.open(
            'https://stacapi-cdos.apps.okd.crocc.meso.umontpellier.fr',
            modifier=dinamis_sdk.sign_inplace,
        )
        res = api.search(
            collections=["spot-6-7-drs"],
            bbox=[4.09, 43.99, 5, 44.01],
            datetime=['2020-01-01', '2021-01-01']
        )
        for item in res.items():
            sc = from_stac(item)
            assert isinstance(sc, Spot67DRSScene)


if __name__ == '__main__':
    unittest.main()
