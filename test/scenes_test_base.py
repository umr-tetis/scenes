# -*- coding: utf-8 -*-
import filecmp
import pyotb
import unittest
from abc import ABC

from scenes import utils


class ScenesTestBase(ABC, unittest.TestCase):
    """
    Base class for tests
    """

    def compare_images(self, image, reference, roi=None, mae_threshold=0.01):
        """
        Compare one image (typically: an output image) with a reference
        :param image: image to compare
        :param reference: baseline
        :param roi: Region of interest [startx, starty, sizex, sizey]
        :param mae_threshold: mean absolute error threshold
        :return: boolean
        """
        nbands_in = pyotb.Input(image).shape[-1]
        nbands_ref = pyotb.Input(reference).shape[-1]

        assert nbands_in == nbands_ref, f"Image has {nbands_in} but baseline has {nbands_ref}"

        for i in range(1, nbands_ref + 1):
            dic = {
                'ref.in': reference,
                'ref.channel': i,
                'meas.in': image,
                'meas.channel': i
            }
            if roi:
                dic.update({
                    "roi.startx": roi[0],
                    "roi.starty": roi[1],
                    "roi.sizex": roi[2],
                    "roi.sizey": roi[3]
                })
            comp = pyotb.CompareImages(dic)
            mae = comp.GetParameterFloat('mae')
            assert mae < mae_threshold, f"MAE is {mae} > {mae_threshold}\n" \
                                        f"\nTarget summary:\n{utils.pprint(image.summarize())}\n" \
                                        f"\nReference summary:\n{utils.pprint(reference.summarize())}"

    def compare_file(self, file, reference):
        """
        Compare two files

        Args:
            file: file to compare
            reference: baseline

        Return:
             a boolean
        """
        assert filecmp.cmp(file,
                           reference), f"File {file} differs with baseline ({reference})"
