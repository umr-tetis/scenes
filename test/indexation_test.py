# -*- coding: utf-8 -*-
from scenes_test_base import ScenesTestBase
import scenes
import tests_data

sc = scenes.spot.get_local_spot67drs_scene(dimap_xs=tests_data.DIMAP1_XS, dimap_pan=tests_data.DIMAP1_P)


class ImageryTest(ScenesTestBase):

    def test_scene1_indexation(self):
        index = scenes.Index(scenes_list=[sc])
        for find in (index.find, index.find_indices):
            self.assertTrue(find(scenes.BoundingBox(xmin=4.317, xmax=4.420, ymin=43.706, ymax=43.708)))
            self.assertFalse(find(scenes.BoundingBox(xmin=3.000, xmax=3.001, ymin=43.000, ymax=43.001)))
            self.assertTrue(find(scenes.vector.get_bbox_wgs84(tests_data.ROI_MTP_4326)))
            self.assertTrue(find(scenes.vector.get_bbox_wgs84(tests_data.ROI_MTP_2154)))
            self.assertTrue(find(tests_data.ROI_MTP_4326))
            self.assertTrue(find(tests_data.ROI_MTP_2154))

    def test_epsg(self):
        self.assertTrue(sc.epsg == 2154)


if __name__ == '__main__':
    unittest.main()
