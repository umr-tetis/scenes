"""
This module provides mechanisms to enable pyotb raster caching on the local
filesystem.
"""
from __future__ import annotations
import hashlib
import json
import os
import tempfile
import pyotb


class Cache(pyotb.Input):
    """
    Enable to manage a given pipeline output, depending on if it's already in
    the cache.
    """

    def __init__(
            self,
            pyotb_output: pyotb.Output,
            temporary_directory: str = None,
            output_parameter_key: str = None,
            extension: str = None,
            pixel_type: str = None,
            summary_modifier=None
    ):
        """
        Initialize the cache.

        Args:
            pyotb_output: a pyotb.Output instance.
            temporary_directory: a temporary directory for the cached files.
                Default is system temp directory.
            output_parameter_key: output parameter key (default is first key)
            extension: file extension (default: .tif)
            pixel_type: pixel type
            summary_modifier: optional function to modify the summary
        """
        # Get app
        pyotb_app = pyotb_output.pyotb_app

        # Get summary
        summary = pyotb_app.summarize()  # need pyotb >= 1.5.1

        # Modify summary
        if summary_modifier:
            summary = summary_modifier(summary)

        # Summary --> md5sum
        desc = json.dumps(summary)
        md5sum = hashlib.md5(desc.encode('utf-8')).hexdigest()

        # App name
        app_name = summary["name"]

        # Cache filename
        if not output_parameter_key:
            output_parameter_key = pyotb_app.output_param
        if not extension:
            extension = ".tif?&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES"
        elif not extension.startswith("."):
            extension = f".{extension}"
        if not pixel_type:
            pixel_type = "float"
        if not temporary_directory:
            temporary_directory = tempfile.gettempdir()
        if not os.path.exists(temporary_directory):
            os.makedirs(temporary_directory, exist_ok=True)
        prefix = os.path.join(
            temporary_directory, f"{app_name}_{output_parameter_key}_{md5sum}"
        )
        cache_file = f"{prefix}{extension}"
        json_file = f"{prefix}.json"

        # Check which cache files already exist
        if not os.path.exists(json_file):
            # pyotb write
            pyotb_output.write(cache_file, pixel_type=pixel_type)
            # json
            with open(json_file, 'w', encoding='utf-8') as file:
                json.dump(summary, file, ensure_ascii=False, indent=4)

        super().__init__(filepath=cache_file.split("?&")[0])
