"""
This module enables various operations from STAC catalogs.

# Spot-6/7 DRS from Dinamis (experimental)

```python
import dinamis_sdk
from stac_client import Client

api = Client.open(
    'https://stacapi-dinamis.apps.okd.crocc.meso.umontpellier.fr',
    modifier=dinamis_sdk.sign_inplace,
)
res = api.search(
    collections=["spot-6-7-drs"],
    bbox=[4.09, 43.99, 5, 44.01],
    datetime=['2020-01-01', '2021-01-01']
)

for item in res.items():
    sc = from_stac(item)
    assert isinstance(sc, Spot67DRSScene)
    print(sc)
```

# Sentinel-2 images from Microsoft planetary computer

```python
import planetary_computer
from stac_client import Client

api = Client.open(
    'https://planetarycomputer.microsoft.com/api/stac/v1',
    modifier=planetary_computer.sign_inplace,
)
res = api.search(
    collections=["sentinel-2-l2a"],
    bbox=[4.99, 43.99, 5, 44.01],
    datetime=['2020-01-01', '2020-01-08']
)

for item in res.items():
    sc = from_stac(item)
    assert isinstance(sc, Sentinel2MPCScene)
    print(sc)
```

"""
from __future__ import annotations
from typing import Dict, List
from urllib.parse import urlparse
import pystac

from scenes.core import Scene
from scenes.sentinel import Sentinel2MPCScene
from scenes.spot import Spot67DRSScene

vsicurl_media_types = [
    pystac.MediaType.GEOPACKAGE,
    pystac.MediaType.GEOJSON,
    pystac.MediaType.COG,
    pystac.MediaType.GEOTIFF,
    pystac.MediaType.TIFF,
    pystac.MediaType.JPEG2000
]


def _get_asset_path(asset: pystac.asset) -> str:
    """
    Return the URI suited for GDAL if the asset is some geospatial data.
    If the asset.href starts with "http://", "https://", etc. then the
    returned URI is prefixed with "/vsicurl".

    Args:
        asset: STAC asset

    Returns:
        URI, with or without "/vsicurl/" prefix

    """
    protocols = ["http://", "https://"]
    url = asset.href
    if asset.media_type in vsicurl_media_types:
        if any(url.lower().startswith(prefix) for prefix in protocols):
            return f"/vsicurl/{url}"
    return url


def _get_assets_paths(
        item: pystac.Item,
        domains: List[str],
        ids_prefixes: List[str]
) -> Dict[str, str]:
    """
    Return the dict of assets paths.

    Args:
        item: STAC item
        domains: authorized domains
        ids_prefixes: authorized STAC item ID prefixes

    Returns:
        assets paths

    """
    ids_cond = any(item.id.startswith(v) for v in ids_prefixes)
    url_cond = all(
        any(
            urlparse(asset.href).netloc.endswith(domain)
            for domain in domains
        )
        for asset in item.assets.values()
    )
    if ids_cond and url_cond:
        assets_paths = {
            key: _get_asset_path(asset)
            for key, asset in item.assets.items()
        }
        return assets_paths
    return None


def from_stac(item: pystac.Item) -> Scene:
    """
    Return a `Scene` object from a STAC item.

    Args:
        item: STAC item

    Returns:
        a `Scene` instance

    """
    # S2 images from Microsoft Planetary
    assets_paths = _get_assets_paths(
        item=item,
        domains=["microsoft.com", "windows.net"],
        ids_prefixes=["S2A_MSIL2A", "S2B_MSIL2A"]
    )
    if assets_paths:
        return Sentinel2MPCScene(assets_paths=assets_paths)

    # Spot 6/7 images from Dinamis
    assets_paths = _get_assets_paths(
        item=item,
        domains=["dinamis.apps.okd.crocc.meso.umontpellier.fr"],
        ids_prefixes=["SPOT"]
    )
    if assets_paths:
        return Spot67DRSScene(assets_paths=assets_paths)

    raise ValueError("No Scene class for this stac item")
