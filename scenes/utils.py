"""
This module contains a set of generic purpose helpers
"""
from __future__ import annotations
from typing import List
import os
import glob
import pathlib
import zipfile
import re
import fnmatch
import json
import requests
from tqdm.autonotebook import tqdm


def find_files_in_all_subdirs(
        pth: str,
        pattern: str,
        case_sensitive: bool = True
) -> List[str]:
    """
    Returns the list of files matching the pattern in all subdirectories of pth

    Args:
        pth: path
        pattern: pattern
        case_sensitive: boolean (Default value = True)

    Returns:
        list of str

    """
    reg_expr = re.compile(
        fnmatch.translate(pattern), 0 if case_sensitive else re.IGNORECASE
    )
    res = []
    for root, _, files in os.walk(pth, topdown=True):
        res += [os.path.join(root, j) for j in files if re.match(reg_expr, j)]
    return res


def find_file_in_dir(pth: str, pattern: str) -> List[str]:
    """
    Returns the list of files matching the pattern in the input directory.

    Args:
        pth: path
        pattern: pattern

    Returns:
        list of str

    """
    return glob.glob(os.path.join(pth, pattern))


def get_parent_directory(pth: str) -> str:
    """
    Return the parent directory of the input directory or file.

    Args:
        pth: input directory or file

    Returns:
        parent directory

    """
    path = pathlib.Path(pth)
    if not path:
        raise FileNotFoundError(f"Cannot define path: {path}")
    return str(path.parent)


def list_files_in_zip(filename: str, endswith: str = None) -> List[str]:
    """
    List files in zip archive.

    Args:
        filename: path of the zip
        endswith: optional, end of filename to be matched (Default value is
         None)

    Returns:
        list of filepaths

    """
    with zipfile.ZipFile(filename) as zip_file:
        filelist = zip_file.namelist()
    if endswith:
        filelist = [f for f in filelist if f.endswith(endswith)]

    return filelist


def to_vsizip(zipfn: str, relpth: str) -> str:
    """
    Create path from zip file.

    Args:
        zipfn: zip archive
        relpth: relative path (inside archive)

    Returns:
        vsizip path

    """
    return f"/vsizip/{zipfn}/{relpth}"


def basename(pth: str) -> str:
    """
    Returns the basename. Works with files and paths.

    Args:
        pth: path

    Returns:
        basename of the path

    """
    return str(pathlib.Path(pth).name)


def pprint(dic: dict) -> str:
    """
    Returns a str pretty-printing the input dict.

    Args:
        dic: dictionary

    Returns:
        string

    """
    return json.dumps(dic, sort_keys=True, indent=4)


def download(url: str, out_filename: str, nb_retries=5):
    """
    Download a remote file.

    Args:
        url: url of the file to download
        out_filename: local file name for the downloaded file
        nb_retries: number of retries

    """
    for attempt in range(nb_retries):
        resp = requests.get(url, stream=True, timeout=5)
        try:
            tot_size_in_bytes = int(resp.headers.get('content-length', 0))
            block_size = 32 * 1024  # 32 Kb
            pbar = tqdm(total=tot_size_in_bytes, unit='iB', unit_scale=True)
            with open(out_filename, 'wb') as file:
                for data in resp.iter_content(block_size):
                    pbar.update(len(data))
                    file.write(data)
            pbar.close()
        except requests.exceptions.RequestException as err:
            print(f"Warning: download has failed ({err}), "
                  f"(attempt {attempt + 1} / {nb_retries})")
            if attempt == nb_retries - 1:
                raise
