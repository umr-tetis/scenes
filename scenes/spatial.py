"""
This module provides classes and functions to help with light geospatial
objects (projections, bounding boxes, etc).
"""
from __future__ import annotations
from typing import List, Tuple
import math
from osgeo import osr, ogr


def coords2poly(coords: Tuple[Tuple[float]]) -> ogr.Geometry:
    """
    Converts a list of coordinates into a polygon

    Args:
        coords: tuple of (x, y) coordinates

    Returns:
        a polygon

    """
    ring = ogr.Geometry(ogr.wkbLinearRing)
    for coord in coords + (coords[0],):
        x, y = coord
        ring.AddPoint(x, y)
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)

    return poly


class BoundingBox:
    """
    The bounding box class
    """

    def __init__(self, xmin: float, ymin: float, xmax: float, ymax: float):
        """
        Args:
            xmin: Lower value on the x-axis
            ymin: Lower value on the y-axis
            xmax: Higher value on the x-axis
            ymax: Higher value on the y-axis
        """
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax

    def union(self, other: BoundingBox) -> BoundingBox:
        """
        Return a new bounding box resulting in the union of self and other

        Args:
            other: another bounding box

        Returns:
            a new bounding box

        """
        return BoundingBox(xmin=min(self.xmin, other.xmin),
                           ymin=min(self.ymin, other.ymin),
                           xmax=max(self.xmax, other.xmax),
                           ymax=max(self.ymax, other.ymax))

    def __str__(self) -> str:
        """
        Returns a string representing the object

        Returns:
            str(self.to_list())

        """
        return str(self.to_list())

    def to_list(self) -> List[float]:
        """
        Converts the bbox into a list of coordinates, like rasterio does.

        Returns:
            [xmin, ymin, xmax, ymax]

        """
        return [self.xmin, self.ymin, self.xmax, self.ymax]

    def to_ogrgeom(self) -> ogr.Geometry:
        """
        Converts the BoundingBox into an OGR geometry

        Returns: an OGR Geometry from the bounding box

        """
        coords = ((self.xmin, self.ymin),
                  (self.xmax, self.ymin),
                  (self.xmax, self.ymax),
                  (self.xmin, self.ymax))
        return coords2poly(coords)


def poly_overlap(poly: ogr.Geometry, other_poly: ogr.Geometry) -> float:
    """
    Returns the ratio of polygons overlap.

    Args:
        poly: polygon
        other_poly: other polygon

    Returns:
        overlap (in the [0, 1] range). 0 -> no overlap with other_poly,
            1 -> poly is completely inside other_poly

    """
    inter = poly.Intersection(other_poly)

    return inter.GetArea() / poly.GetArea()


def epsg2srs(epsg: int) -> osr.SpatialReference:
    """
    Return a Spatial Reference System corresponding to an EPSG

    Args:
        epsg: EPSG (int)

    Returns:
        OSR spatial reference

    """
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(epsg)
    return srs


def coordinates_transform(
        src_srs: osr.SpatialReference,
        tgt_srs: osr.SpatialReference
) -> osr.CoordinateTransformation:
    """
    Return a coordinates transform.

    Args:
        src_srs: source CRS
        tgt_srs: target CRS

    Returns:
        osr.CoordinateTransformation

    """
    assert src_srs, "reproject_coords: src_srs is None!"
    assert tgt_srs, "reproject_coords: tgt_srs is None!"
    # Use x,y = lon, lat
    src_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    tgt_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    return osr.CoordinateTransformation(src_srs, tgt_srs)


def reproject_coords(
        coords: List[Tuple[float]],
        src_srs: osr.SpatialReference,
        tgt_srs: osr.SpatialReference
) -> Tuple[Tuple[float]]:
    """
    Reproject a list of x,y coordinates.

    Args:
        coords: list of (x, y) tuples
        src_srs: source CRS
        tgt_srs: target CRS

    Returns:
        trans_coords: coordinates in target CRS

    """
    trans_coords = []
    transform = coordinates_transform(src_srs=src_srs, tgt_srs=tgt_srs)
    for x, y in coords:
        x, y, _ = transform.TransformPoint(x, y)
        trans_coords.append((x, y))

    return tuple(trans_coords)


def coord_list_to_bbox(coords: Tuple[Tuple[float]]) -> BoundingBox:
    """
    Computes the bounding box of multiple coordinates

    Args:
        coords: coordinates (x1, y1), ..., (xN, yN)

    Returns:
        BoundingBox instance

    """
    xmin, xmax = math.inf, -math.inf
    ymin, ymax = math.inf, -math.inf
    for x, y in coords:
        xmin = min(xmin, x)
        xmax = max(xmax, x)
        ymin = min(ymin, y)
        ymax = max(ymax, y)

    return BoundingBox(xmin=xmin, ymin=ymin, xmax=xmax, ymax=ymax)


def extent_overlap(
        extent: Tuple[Tuple[float]],
        other_extent: Tuple[Tuple[float]]
) -> float:
    """
    Returns the ratio of extents overlaps.

    Args:
        extent: extent
        other_extent: other extent

    Returns:
        overlap (in the [0, 1] range). 0 -> no overlap with other_extent,
            1 -> extent lies inside other_extent

    """
    poly = coords2poly(extent)
    other_poly = coords2poly(other_extent)
    return poly_overlap(poly=poly, other_poly=other_poly)
