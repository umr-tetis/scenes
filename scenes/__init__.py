# -*- coding: utf-8 -*-
"""
This module helps to process local remote sensing products
"""
from os.path import dirname, basename, isfile, join
import glob
import importlib
import pkg_resources
from .core import load_scenes, save_scenes  # noqa: 401
from .indexation import Index  # noqa: 401
from .spatial import BoundingBox  # noqa: 401
modules = glob.glob(join(dirname(__file__), "*.py"))
for f in modules:
    if isfile(f) and f.endswith('.py') and not f.endswith('__init__.py'):
        importlib.import_module(f".{basename(f)[:-3]}", __name__)
__version__ = pkg_resources.require("scenes")[0].version
