#!/usr/bin/env python3
"""
This application enables to perform a spatial query over a collection of image, from the specific ROI (vector or raster)

With a vector:
```
search.py
  --pickle_file s2_collection
  --roi toto.gpkg
```

With a raster:

```
search.py
  --pickle_file s2_collection
  --roi toto.tif
```

"""
import sys
import argparse
from scenes import load_scenes, Index, raster, vector

def main(args):
    # Arguments
    parser = argparse.ArgumentParser(description="Search scenes intersecting an ROI",)
    parser.add_argument("--pickle_file", help="List of scenes (serialized in pickle format)", required=True)
    parser.add_argument("--roi", help="ROI. Can be a vector or a raster", required=True)
    params = parser.parse_args(args)

    # Load scenes list
    scenes = load_scenes(params.pickle_file)

    # spatial index
    print("Indexation...")
    idx = Index(scenes)

    # search
    print("Search roi")
    bbox_fn = raster.get_bbox_wgs84 if params.roi.lower().endswith(".tif") else vector.get_bbox_wgs84
    matches = idx.find(bbox_wgs84=bbox_fn(params.roi))
    print(f"{len(matches)} scenes found.")
    for i, scene_match in enumerate(matches):
        print(f"Scene #{i}")
        print(scene_match)


if __name__ == "__main__":

    main(sys.argv[1:])