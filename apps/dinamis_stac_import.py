#!/usr/bin/env python3
"""
This application enables to search existing Spot-6/7 products from the dinamis STAC catalog, and save a pickle object
containing all the `scene.spot.Spot67Scene` instances

``` command-line
dinamis_stac_import
  --roi_vec roi.gpkg
  --out_pickle collection
```

"""
import sys
import argparse
import scenes

def main(args):
    # Arguments
    parser = argparse.ArgumentParser(description="Import Spot-6/7 images from DRS into a list of scenes (pickle)",)
    parser.add_argument("--roi_vec", help="Root directories containing MS and PAN folders", required=True)
    parser.add_argument("--out_pickle", help="Output pickle file", required=True)
    params = parser.parse_args(args)

    # Get vector bbox
    bbox_wgs84 = scenes.vector.get_bbox_wgs84(params.roi_vec)
    dinamis_provider = scenes.stac.DinamisSpot67Provider()
    scs = dinamis_provider.scenes_search(bbox_wgs84=bbox_wgs84)

    # Save scenes in a pickle file
    scenes.save_scenes(scs, params.out_pickle)


if __name__ == "__main__":

    main(sys.argv[1:])