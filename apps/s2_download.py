#!/usr/bin/env python3
"""
This application enables to download all Sentinel-2 images overlapping a reference raster, and acquired at a specific
 date, from the Theia land data center.
The Theia credential have to be provided.

``` command-line
s2_download
  --refimage raster.tif
  --theia_cfg ~/cfg.txt
  --download_dir /tmp
  --year 2022 --month 12 --day 1
```

With `~/cfg.txt`:

```
serveur = https://theia.cnes.fr/atdistrib
resto = resto2
token_type = text
login_theia = remi.cresson@irstea.fr
password_theia = thisisnotmyrealpassword
```


"""
import sys
import argparse
from scenes import TheiaDownloader, raster
import datetime

def main(args):
    # Arguments
    parser = argparse.ArgumentParser(description="Download test",)
    parser.add_argument("--refimage", required=True)
    parser.add_argument("--theia_cfg", required=True)
    parser.add_argument("--download_dir")
    parser.add_argument("--year", type=int, default=2020)
    parser.add_argument("--month", type=int, default=1)
    parser.add_argument("--day", type=int, default=1)
    params = parser.parse_args(args)

    # Get all scenes in the root_dir
    bbox = raster.get_bbox_wgs84(params.refimage)
    acq_date = datetime.datetime(year=params.year, month=params.month, day=params.day)
    downloader = TheiaDownloader(config_file=params.theia_cfg)
    downloader.download_closest(bbox_wgs84=bbox, acq_date=acq_date, download_dir=params.download_dir)

if __name__ == "__main__":

    main(sys.argv[1:])