#!/usr/bin/env python3
"""
This application enables to search existing Spot-6/7 products from different locations, and save a pickle object
containing all the `scene.spot.Spot67Scene` instances

``` command-line
drs_spot67_import
  --root_dirs /ortho_drs/2018/ /ortho_drs/2019
  --out_pickle collection
```

"""
import sys
import argparse
from scenes import save_scenes
from scenes.spot import get_spot67_scenes

def main(args):
    # Arguments
    parser = argparse.ArgumentParser(description="Import Spot-6/7 images from DRS into a list of scenes (pickle)",)
    parser.add_argument("--root_dirs", nargs='+', help="Root directories containing MS and PAN folders", required=True)
    parser.add_argument("--out_pickle", help="Output pickle file", required=True)
    params = parser.parse_args(args)

    # Get all scenes in the root_dir
    scenes = []
    for root_dir in params.root_dirs:
        scenes += get_spot67_scenes(root_dir)

    # Save scenes in a pickle file
    save_scenes(scenes, params.out_pickle)


if __name__ == "__main__":

    main(sys.argv[1:])