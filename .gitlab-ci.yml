default:
  image: "gitlab-registry.irstea.fr/remi.cresson/otbtf:3.4.0-cpu-dev"
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - .cache/pip/
      - venv/
      - test_data/

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  TEST_DATA_DIR: "$CI_PROJECT_DIR/test_data"

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID               # Execute jobs in merge request context
    - if: $CI_COMMIT_BRANCH == 'develop'     # Execute jobs when a new commit is pushed to default branch

stages:
  - Environment
  - Static Analysis
  - Install
  - Documentation
  - Test

# --------------------------------------------- Init venv --------------------------------------------------------------

make_env:
  stage: Environment
  before_script:
    - sudo apt update && sudo apt install -y virtualenv
  script:
    - virtualenv --system-site-packages venv
    - source venv/bin/activate
    - pip install --upgrade pip
    - pip install flake8 pylint codespell pytest pytest-cov
    - wget -P . --no-verbose -e robots=off --recursive --level=inf --no-parent -R "index.html*" -R "LOGO.JPG" --cut-dirs=3 --no-host-directories --content-on-error http://indexof.montpellier.irstea.priv/projets/geocicd/scenes/test_data/


# --------------------------------------------- Static analysis --------------------------------------------------------

.static_analysis_base:
  stage: Static Analysis
  allow_failure: true
  before_script:
    - source venv/bin/activate

flake8:
  extends: .static_analysis_base
  script:
    - flake8 $PWD/scenes

pylint:
  extends: .static_analysis_base
  script:
    - pylint --good-names=i,x,y --ignored-modules=pyotb --disable=too-many-arguments $PWD/scenes

codespell:
  extends: .static_analysis_base
  script: 
    - codespell --skip="*.png,*.template,*.pbs,*.jpg,*git/lfs*,*venv/*,*test_data/*"

# ----------------------------------------------------- Install --------------------------------------------------------

pip_install:
  stage: Install
  before_script:
    - source venv/bin/activate
  script:
    - pip install .

# ------------------------------------------------------- Doc ----------------------------------------------------------

.doc_base:
  stage: Documentation
  before_script:
    - source venv/bin/activate
    - pip install mkdocstrings mkdocstrings[crystal,python] mkdocs-material mkdocs-gen-files mkdocs-section-index mkdocs-literate-nav mkdocs-mermaid2-plugin --upgrade
  artifacts:
    paths:
      - public
      - public_test

test:
  extends: .doc_base
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
  script:
    - mkdocs build --site-dir public_test

pages:
  extends: .doc_base
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
  script:
    - mkdocs build --site-dir public

# ------------------------------------------------------ Test ----------------------------------------------------------

.test_base:
  stage: Test
  before_script:
    - source venv/bin/activate
    - mkdir tests_artifacts

spatial:
  extends: .test_base
  script:
    - pytest -o log_cli=true --log-cli-level=INFO --junitxml=report_spatial_test.xml test/spatial_test.py

spot-6/7 imagery:
  extends: .test_base
  script:
    - pytest -o log_cli=true --log-cli-level=INFO --junitxml=report_imagery_test.xml test/spot67_imagery_test.py

sentinel-2 theia imagery:
  extends: .test_base
  script:
    - pytest -o log_cli=true --log-cli-level=INFO --junitxml=report_imagery_test.xml test/sentinel2_theia_test.py

indexation:
  extends: .test_base
  script:
    - pytest -o log_cli=true --log-cli-level=INFO --junitxml=report_indexation_test.xml test/indexation_test.py

dates:
  extends: .test_base
  script:
    - pytest -o log_cli=true --log-cli-level=INFO --junitxml=report_dates_test.xml test/dates_test.py

stac:
  extends: .test_base
  script:
    - pip install planetary-computer
    - pip install https://gitlab.irstea.fr/dinamis/dinamis-sdk/-/archive/main/dinamis-sdk-main.zip
    - pytest -o log_cli=true --log-cli-level=INFO --junitxml=report_stac_test.xml test/stac_test.py

serialization:
  extends: .test_base
  script:
    - pytest -o log_cli=true --log-cli-level=INFO --junitxml=serialization_test.xml test/serialization_test.py
