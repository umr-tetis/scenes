# -*- coding: utf-8 -*-
import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="scenes",
    version="1.6.4",
    author="Rémi Cresson",
    author_email="remi.cresson@inrae.fr",
    description="Library to ease the processing of remote sensing products",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.irstea.fr/remi.cresson/scenes",
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    install_requires=[
        "rtree",
        "pyotb==1.5.4",
        "requests",
        "tqdm",
        "pystac-client",
        "theia-picker",
    ],
    keywords=["remote sensing", "otb", "orfeo toolbox", "pyotb", "stac"],
    scripts=[
        "apps/drs_spot67_import.py",
        "apps/s2_download.py",
        "apps/s2_import.py",
        "apps/search.py"
    ]
)
