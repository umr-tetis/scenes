import scenes
import pyotb


def main():

    bbox = scenes.BoundingBox(xmin=2, ymin=45.23, xmax=2.1, ymax=45.26)
    provider = scenes.stac.MPCProvider()
    results = provider.stac_search(bbox_wgs84=bbox)
    for i, result in enumerate(results):
        print(result)
        sc = provider.stac_item_to_scene(result)
        dyn = pyotb.DynamicConvert(sc.get_10m_bands()[0:256, 0:256, :])
        dyn.write(f"/tmp/{i}.png")


if __name__ == "__main__":
    main()
