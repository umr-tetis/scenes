import scenes
import pyotb


def main():

    bbox = scenes.BoundingBox(xmin=2, ymin=45.23, xmax=2.1, ymax=45.26)
    provider = scenes.stac.DinamisSpot67Provider()
    scs = provider.scenes_search(bbox_wgs84=bbox)
    for i, sc in enumerate(scs):
        print(sc)
        pyotb.HaralickTextureExtraction({"in": sc.get_xs(), "out": f"/data/tex_{i}.img"})


if __name__ == "__main__":
    main()
